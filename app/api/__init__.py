from fastapi import APIRouter

from .users_api import *
from .account_api import *


api_router = APIRouter()

api_router.include_router(users_api.router, prefix='/users', tags=['Users'])
api_router.include_router(account_api.router, prefix='/acc', tags=['Account'])



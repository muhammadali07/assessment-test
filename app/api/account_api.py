import structlog
from datetime import datetime
from fastapi import Depends, APIRouter
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Optional
from service import get_async_session, verify_token, configure_structlog
from repository.schema import RegistAcc, postTransaction
from logic import account_logic

configure_structlog()

logger = structlog.get_logger()

router = APIRouter()

@router.post('/regist-acc')
async def regist_acc(
    data: RegistAcc,
    user_info : dict = Depends(verify_token),
    db: AsyncSession = Depends(get_async_session)
):
    logger.info("register acc", request=data)
    out_resp = await account_logic.create_regist_acc(data, user_info, db)
    return out_resp


@router.post('/deposit')
async def deposit_balance(
    data: postTransaction,
    user_info : dict = Depends(verify_token),
    db: AsyncSession = Depends(get_async_session)
):
    logger.info("Deposito transaction", request=data)
    out_resp = await account_logic.deposit_balance(data,user_info, db)
    return out_resp

@router.post('/cash-withdrawal')
async def cash_withdrawal(
    data: postTransaction,
    user_info : dict = Depends(verify_token),
    db: AsyncSession = Depends(get_async_session)
):
    logger.info("Cash Withdraw transaction", request=data)
    out_resp = await account_logic.cash_withdrawal(data, user_info, db)
    return out_resp

@router.get('/balance/{account_number}')
async def balance(
    account_number: str,
    user_info : dict = Depends(verify_token),
    db: AsyncSession = Depends(get_async_session)
):
    logger.debug("Getting balance account", request=account_number)
    out_resp = await account_logic.balance(account_number, db)
    return out_resp

@router.get('/mutation/{account_number}')
async def mutation(
    account_number: str,
    user_info : dict = Depends(verify_token),
    mutation: Optional[str] = None,
    db: AsyncSession = Depends(get_async_session)
):
    logger.debug("Getting mutation account", request=account_number)
    out_resp = await account_logic.mutation(account_number,mutation, db)
    return out_resp
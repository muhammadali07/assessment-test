import structlog

from fastapi import Depends, APIRouter
from sqlalchemy.ext.asyncio import AsyncSession

from service import get_async_session, configure_structlog
from repository.schema import Users, UsersInLogIn

from logic import users_logic

configure_structlog()

logger = structlog.get_logger()

router = APIRouter()


@router.post(
    "/create-new-users",
)
async def create_new_user(
    data: Users, 
    db: AsyncSession = Depends(get_async_session)
):
    logger.info("Create New User", request=data)
    out_resp = await users_logic.create_user(data, db)
    return out_resp


@router.post("/login", )
async def login(
    data: UsersInLogIn, 
    db: AsyncSession = Depends(get_async_session)
):
    logger.info("Login", request=data.email)
    out_resp = await users_logic.login(
        data, db
    )
    return out_resp



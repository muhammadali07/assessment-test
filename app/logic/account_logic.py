import structlog
from sqlalchemy.ext.asyncio import AsyncSession
from service import response, configure_structlog
from repository.schema import RegistAcc, postTransaction
from repository import account_crud


configure_structlog()

logger = structlog.get_logger()

async def create_regist_acc(data:RegistAcc, user_info:dict, db_session: AsyncSession) -> response:
    async with db_session as session:
        try:
            logger.info("getting role user", result=user_info['role'])
            if user_info['role'] != 'teller':
                raise Exception(f"user {user_info['username']} has not access this transction")
            
            logger.debug("validasi account exist", result=data)
            valData = await account_crud.data_account(
                data.no_identity, data.phone, session
            )

            if valData not in (None, []):
                return response(
                    message_id="02",
                    status= f"request with no identity {data.no_identity} has been registered with account number {valData.get('account_number', '')}"
                )
            
            logger.info("result validation data", result=valData,)
            res = await account_crud.create_regist_acc(data, user_info, session)

            logger.info("result create regist account", result=res['data'])

            await session.commit()
            return res
        except (Exception, ValueError) as e:
            logger.error(f"Exception Error: {e}")
            return response(
                message_id="02",
                status=f"{e}",
                data=None
            )
        

async def deposit_balance(data:postTransaction, user_info:dict,  db_session: AsyncSession) -> response:
    async with db_session as session:
        try:
            if data.nominal <= 0:
                raise Exception("Nominal must be greater than Rp. 0,0 ")

            trx = await account_crud.deposit_balance(data, user_info, session)
            
            logger.info("Deposit Transaction", result=trx['status'])
            
            await session.commit()
            return trx
        except (Exception, ValueError) as e:
            logger.error(f"Exception Error: {e}")
            return response(
                message_id="02",
                status=f"{e}",
                data=None
            )
         

async def cash_withdrawal(data:postTransaction, user_info:dict,  db_session: AsyncSession) -> response:
    async with db_session as session:
        try:
            if data.nominal <= 0:
                raise Exception("Nominal must be greater than Rp. 0,0 ")

            trx = await account_crud.cash_withdrawal(data, user_info, session)
            logger.info("Cash withdraw transaction", result=trx['status'])
            await session.commit()
            return trx
        except (Exception, ValueError) as e:
            logger.error(f"Exception Error: {e}")
            return response(
                message_id="02",
                status=f"{e}",
                data=None
            )



async def balance(account_number:str, db_session: AsyncSession) -> response:
    async with db_session as session:
        try:
            if account_number in (None, ''):
                raise Exception("account number does not send in request ")

            trx = await account_crud.balance_acount(account_number, session)
            logger.info("Balance Amount Transaction", result="success")

            await session.commit()
            return response(
                "00", f"balance amount with account number {account_number} Rp.{trx}", trx
            )
        except (Exception, ValueError) as e:
            logger.error(f"Exception Error: {e}")
            return response(
                message_id="02",
                status=f"{e}",
                data=None
            )
        



async def mutation(account_number:str, mutation:str, db_session: AsyncSession) -> response:
    async with db_session as session:
        try:
            if account_number in (None, ''):
                raise Exception("account number does not send in request ")

            if mutation not in (None, ''):
                
                if len(mutation) > 1 or mutation.upper() not in ("C", "D"):
                    raise Exception("Mutation type can only be 1 character (C/D)")

            data = await account_crud.mutation(account_number,mutation, session)

            logger.info("Mutation List", result="success")

            await session.commit()
            status = f"data mutation of account number {account_number}" if len(data) > 0 else "data mutation not found"
            return response(
                "00", status, data
            )
        except (Exception, ValueError) as e:
            logger.error(f"Exception Error: {e}")
            return response(
                message_id="02",
                status=f"{e}",
                data=None
            )
        


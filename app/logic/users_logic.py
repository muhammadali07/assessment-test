from datetime import datetime, timedelta
from sqlalchemy.ext.asyncio import AsyncSession
from service import utils, response, create_access_token
from repository.schema import Users, UsersInLogIn
from repository import users_crud

ACCESS_TOKEN_EXPIRE_MINUTES = 30

async def create_user(data:Users, db_session: AsyncSession) -> response:
    async with db_session as session:
        try:
            data.email = utils.validate_email(data.email)
            data.password = utils.validate_password(data.password)

            res = await users_crud.create_new_user(data, session)
            
            await session.commit()
        
            return res
        except (Exception, ValueError) as e:
            return  response(
                message_id="02",
                status=f"{e}",
                data=None
            )
        

async def login(request: UsersInLogIn , db_session: AsyncSession):
    async with db_session as session:
        try:
            
            getUser = await users_crud.data_user(request.email, session)
            
            if not getUser:
                return response(
                    message_id="02",
                    status="user not found",
                    data=[]
                )
            if request.password != getUser['password']:
                return response(
                    message_id="03",
                    status="Password is wrong",
                    data=[]
                )
            
            dt = {
                    "username": getUser['username'],
                    "password": getUser['password'],
                    "branch_code": getUser['branch_code'],
                    "role": getUser['role'],
                    "access_date": str(datetime.now())
                }
            
            _token = create_access_token(data=dt, expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES))


            return response(message_id="00", status="Login berhasil", data={"token_bearer": _token})
            
        except Exception as e:
            return response(message_id="03", status=f"{str(e)}", data=[])

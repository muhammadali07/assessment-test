import uuid
from fastapi.encoders import jsonable_encoder
from sqlalchemy.future import select
from sqlalchemy import update, or_, and_, desc
from datetime import datetime
from service import response, generate_account_number
from .models import account_models, transaction_models
from .schema import RegistAcc, postTransaction

async def create_regist_acc(data:RegistAcc, user_info:dict, session) -> response:
    try:
        account_number = generate_account_number()
        data = account_models.Account(
            name=data.name,
            no_identity=data.no_identity,
            phone=data.phone,
            account_number=account_number,
            balance=0,
            is_active="T",
            branch_code=user_info['branch_code'],
            user_input=user_info['username'],
            address="Block C7 Surapati Core District",
            created_at=datetime.now()
        )
        session.add(data)
        return response("00", "register account number successfull", {"account_number": account_number})
    
    except Exception as e:
        return response("02", str(e), None)   
    
async def data_account(no_identity: str, phone:None, session) -> response:
    try:
        acc = account_models.Account
        query_stmt = select(acc).filter(
            or_(
                acc.no_identity == no_identity,
                acc.phone == phone
            )
        )  
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().first()
        return jsonable_encoder(res)  
    except Exception as e:
        return response("02", str(e), None)    
    

async def deposit_balance(data:postTransaction, user_info:dict, session) -> response:
    try:
        # update balance account
        _acc = await acc_exist(data.account_number, session)
        if _acc in (None, ''):
            raise Exception(f"accout number {data.account_number} not found")
        
        get_current_balance = await balance_acount(data.account_number, session)

        acc = account_models.Account

        balance_amount = data.nominal + get_current_balance 
        
        update_balance = update(acc).where(acc.account_number==data.account_number).values(balance = balance_amount)

        trx = transaction_models.Transaction
        data = trx(
            idkey=str(uuid.uuid4()),
            type="Credit",
            account_number=data.account_number,
            mutation="C",
            nominal=float(data.nominal),
            branch_code=user_info['branch_code'],
            user_input=user_info['username'],
            created_at=datetime.now()
        )
        session.add(data)

        await session.execute(update_balance)
        
        return response("00", f"saving for account number {data.account_number} successfull, current balance is Rp. {balance_amount}", balance_amount)
    
    except Exception as e:
        return response("02", str(e), None)  
    
    
async def cash_withdrawal(data:postTransaction, user_info:dict, session) -> response:
    try:
        # update balance account after withdraw
        _acc = await acc_exist(data.account_number, session)
        if _acc in (None, ''):
            raise Exception(f"accout number {data.account_number} not found")
        
        acc = account_models.Account

        get_current_balance = await balance_acount(data.account_number, session)
        if get_current_balance < data.nominal:
            raise Exception("insufficient balance")
        
        balance_amount = float(get_current_balance) - float(data.nominal)
        trx = transaction_models.Transaction
        data = trx(
            idkey=str(uuid.uuid4()),
            type="Debit",
            account_number=data.account_number,
            mutation="D",
            nominal=data.nominal,
            branch_code=user_info['branch_code'],
            user_input=user_info['username'],
            created_at=datetime.now()
        )
        session.add(data)

        update_balance = update(acc).where(acc.account_number==data.account_number).values(balance = balance_amount)

        await session.execute(update_balance)
        
        return response("00", f"withdraw for account number {data.account_number} successfull, current balance is Rp. {balance_amount}", balance_amount)
    
    except Exception as e:
        return response("02", str(e), None)  

async def balance_acount(account_number: str, session) -> response:
    try:
        acc = account_models.Account
        query_stmt = select(acc.balance).where(acc.account_number == account_number)
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().first()
        return jsonable_encoder(res)
    except Exception as e:
        return response("02", str(e), None)  

async def acc_exist(account_number: str, session) -> response:
    try:
        acc = account_models.Account
        query_stmt = select(acc.account_number).where(acc.account_number == account_number)
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().first()
        return jsonable_encoder(res)
    except Exception as e:
        return response("02", str(e), None) 
    

async def mutation(account_number: str, mutation:str, session) -> response:
    try:
        trx = transaction_models.Transaction

        terms = []
        if mutation not in (None, ''):
            terms.append(
                or_(
                    trx.mutation.ilike(f"%{mutation.upper()}%")
                )
            )
        if len(terms) > 0:
            query_stmt = select(trx).filter(and_(*(terms),trx.account_number == account_number)).order_by(desc(trx.created_at))    
        else:
            query_stmt = select(trx).where(trx.account_number == account_number).order_by(desc(trx.created_at))
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().all()
        data = []
        for i in res:
            dt = {
                "account_number": i.account_number,
                "transactions_date": i.created_at,
                "code_transactions": "Withdraw" if i.mutation == "D" else "Deposit",
                "type": i.type
            }
            data.append(dt)
        return data
    except Exception as e:
        return response("02", str(e), None)   
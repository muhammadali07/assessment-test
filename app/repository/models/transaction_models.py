from sqlalchemy import Column, String, BigInteger, Text, DateTime, Numeric
from datetime import datetime
from service import Base

class Transaction(Base):
    __tablename__ = 'transaction'
    id = Column(BigInteger, primary_key=True)
    idkey = Column(String(36))
    type = Column(String(50))
    account_number = Column(String(30), nullable=False)
    mutation = Column(String(1))
    nominal = Column(Numeric(28,2))
    branch_code = Column(String(10), nullable=False)
    user_input = Column(String(30))
    created_at = Column(DateTime, default=datetime.now())
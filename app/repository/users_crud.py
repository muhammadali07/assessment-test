from fastapi.encoders import jsonable_encoder
from sqlalchemy.future import select
from datetime import datetime
from service import response
from .models import users_models
from .schema import Users



async def create_new_user(data:Users , session) -> response:
        try:
            val_user_exist = await user_exist(data.email, session)
            if val_user_exist:
                return val_user_exist
            
            data = users_models.Users(
                email=data.email,
                username=data.username,
                password=data.password,
                role="teller",
                branch_code="001",
                created_at=datetime.now()
            )
            session.add(data)
            return response("00", "register user successfull", data)
        
        except Exception as e:
            return response("02", str(e), None)       


async def user_exist(data: str,session):
    try:
        users = users_models.Users
        query_stmt = select(users).where(users.email==data)
        proxy_rows = await session.execute(query_stmt)
        result = proxy_rows.scalars().first()
        if result:
            raise Exception("user has been registered")
        else:
            pass
    
    except Exception as e:
        return response("02", str(e), None)
    

async def data_user(data:str, session):
        try:
            users = users_models.Users
            query_stmt = select(users).where(users.email == data)
            proxy_row = await session.execute(query_stmt)
            result = proxy_row.scalars().first()

            return jsonable_encoder(result)
        
        except Exception as e:
            return response(message_id="03", status=f"{str(e)}", data=[])

import sys
import structlog
import logging
from datetime import datetime
from structlog.stdlib import LoggerFactory
from structlog.processors import (
    StackInfoRenderer,
    TimeStamper,
    format_exc_info,
    JSONRenderer,
    UnicodeDecoder
)

def configure_structlog():
    structlog.configure(
        processors=[
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            StackInfoRenderer(),
            format_exc_info,
            TimeStamper(fmt="iso"),
            UnicodeDecoder(),
            structlog.dev.ConsoleRenderer()
        ],
        logger_factory=LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    formatter = structlog.stdlib.ProcessorFormatter(
        processor=structlog.dev.ConsoleRenderer(colors=True),
        foreign_pre_chain=[structlog.stdlib.add_log_level],
    )

    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)

configure_structlog()

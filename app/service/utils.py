import re
import random

from fastapi import HTTPException

def response(message_id: str, status: str, data=None):
    response = {
        "message_id": message_id,
        "status": status,
        "data": data
    }

    return response


def validate_email(email: str) -> str:
    allowed_domains = ["gmail.com", "hotmail.com", "yahoo.com"]  # Daftar domain yang diizinkan

    regex_pattern = r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
    if not re.match(regex_pattern, email):
        raise ValueError("Incorrect email format")

    domain = email.split("@")[1]
    if domain not in allowed_domains:
        raise ValueError("Email domains not allowed")

    return email

def validate_password(password: str) -> str:
    if len(password) < 5:
        raise ValueError("The password must have a minimum length of 5 characters")

    regex_pattern = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[A-Za-z0-9]+$"
    if not re.match(regex_pattern, password):
        raise ValueError("Password must consist of at least 1 capital letter, 1 lowercase letter, and 1 number")
    
    if any(char.isalnum() is False for char in password):
        raise ValueError("Password cannot contain special characters")
    return password


def generate_account_number():
    account_number = "325"
    for _ in range(7):
        account_number += str(random.randint(0, 9))
    return account_number

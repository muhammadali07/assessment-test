import unittest
import os
from fastapi.testclient import TestClient
from datetime import datetime, timedelta
from main import app
from jose import jwt
from dotenv import load_dotenv

load_dotenv

client = TestClient(app)

endpoint : str = os.getenv('URL_API', 'http://localhost:8888/api/v1/acc')

class TestEndpoints(unittest.TestCase):
    def setUp(self):
        self.token = self.generate_token()  # Generate JWT access token

    def generate_token(self):
        secret_key = "1hs4nxx" 
        algorithm = "HS256"
        payload = {
            "username": "s1276",
            "password": "eS1276",
            "branch_code": "001",
            "role": "teller",
            "access_date": str(datetime.now()),
            "exp": datetime.utcnow() + timedelta(minutes=15)
        } 
        token = jwt.encode(payload, secret_key, algorithm)
        return token

    def test_regist_acc(self):
        data = {                
            "name": "muh ali bakhtiar",
            "no_identity": "3509110701940007",
            "phone": "6281553335534"
        }

        response = client.post(
            f"{endpoint}/regist-acc",
            json=data,
            headers={"Authorization": f"Bearer {self.token}"}  
        )
        print(f"test_regist_acc: {response}")
        

    def test_deposit(self):
        data = {
            "account_number": "3255572881",
            "nominal": 1000
        }
        response = client.post(
            f"{endpoint}/deposit",
            json=data,
            headers={"Authorization": f"Bearer {self.token}"}  
        )
        print(f"test_deposit: {response}")
        

    def test_cash_withdrawal(self):
        data = {
            "account_number": "3255572881",
            "nominal": 1000
        }
        response = client.post(
            f"{endpoint}/cash-withdrawal",
            json=data,
            headers={"Authorization": f"Bearer {self.token}"}  
        )
        print(f"test_cash_withdrawal: {response}")
        

    def test_balance(self):
        account_number = "3255572881"  
        response = client.get(
            f"{endpoint}/balance/{account_number}",
            headers={"Authorization": f"Bearer {self.token}"}  
        )
        print(f"test_balance: {response}")
        

    def test_mutation(self):
        account_number = "3255572881"  
        mutation = ""  
        response = client.get(
            f"{endpoint}/balance/{account_number}",
            params={"mutation": mutation},
            headers={"Authorization": f"Bearer {self.token}"}  
        )
        print(f"test_mutation : {response}")
        

if __name__ == "__main__":
    unittest.main()
